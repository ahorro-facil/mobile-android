package com.MobileAndroid.mobileandroid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileAndroidApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileAndroidApplication.class, args);
	}

}
